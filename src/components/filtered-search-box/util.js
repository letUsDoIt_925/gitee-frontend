export const htmlEscape = (function () {
  const $el = $('<div/>')

  return function (text) {
    return $el.text(text).html()
  }
}())

export function cleanCopy(obj) {
  return JSON.parse(JSON.stringify(obj))
}

export function isEmptyFilterToken($token) {
  return $token.find('.value').length < 1
}

export function isSearchTermToken($token) {
  return $token.hasClass('filtered-search-term')
}

export function findListItem(list, cmp) {
  var target = null

  list.some(function (item) {
    if (cmp(item)) {
      target = item
      return true
    }
    return false
  })
  return target
}

export function findFilterItemByValue(filter, value) {
  var valueStr = value.toString()
  return findListItem(filter.items, function (item) {
    return item.value.toString() === valueStr
  })
}

export function findFilterItemByName(filter, name) {
  if (!filter.items) {
    return null
  }
  return findListItem(filter.items, function (item) {
    return item.name === name
  })
}
