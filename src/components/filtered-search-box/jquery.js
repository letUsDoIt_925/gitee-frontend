/* eslint-disable no-param-reassign */

import { FilteredSearchBox } from './main'

$.fn.filteredSearchBox = function (options) {
  var that = this.data('filteredsearchbox')
  if (that) {
    that.destroy()
  }
  this.data('filteredsearchbox', new FilteredSearchBox(this, options))
  return this
}
