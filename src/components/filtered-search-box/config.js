/* eslint-disable no-console */

export const defaults = {
  filters: [],
  groups: [],
  text: {
    loading: 'loading...',
    placeholder: 'Search or filter results...',
    searchHelp: 'Press Enter or click to search',
    selectOtherFilter: 'Select other filter',
    history: 'Histroy',
    clearHistory: 'Clear history'
  },
  removeIconClass: 'icon times',
  history: {
    limit: 5,
    store: window.localStorage,
    storeKey: 'GiteeSearchHistory'
  },
  data: function () {
    return {}
  },
  callback: function (data) {
    console.log(data)
  }
}

export function Config(config) {
  $.extend(this, defaults, config)
  if (config.text) {
    $.extend(this.text, config.text)
  }
}

export default Config
