module.exports = {
  rules: {
    'quote-props': 0,
    'comma-dangle': 0,
    'spaced-comment': 0,
    'semi': 0,
    'func-names': 0,
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        js: 'never',
        vue: 'never'
      }
    ],
    // 以下规则不适用于 ES5 语法，所以禁用了
    'no-var': 0,
    'prefer-template': 0,
    'prefer-arrow-callback': 0,
    'prefer-destructuring': 0,
    'object-shorthand': 0
  },
  env: {
    browser: true,
    jquery: true,
    node: true
  },
  extends: [
    'airbnb-base'
  ]
};
