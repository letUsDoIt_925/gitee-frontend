/*!
 * Gitee-Frontend.js v0.21.1
 * (c) 2020 Gitee
 * Released under the MIT License.
 */
(function () {
  'use strict';

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function");
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        writable: true,
        configurable: true
      }
    });
    if (superClass) _setPrototypeOf(subClass, superClass);
  }

  function _getPrototypeOf(o) {
    _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return _getPrototypeOf(o);
  }

  function _setPrototypeOf(o, p) {
    _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };

    return _setPrototypeOf(o, p);
  }

  function _assertThisInitialized(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return self;
  }

  function _possibleConstructorReturn(self, call) {
    if (call && (typeof call === "object" || typeof call === "function")) {
      return call;
    }

    return _assertThisInitialized(self);
  }

  var defaults = {
    readonly: false,
    key: 'state',
    name: 'board',
    message: {
      loading: 'loading ...',
      stateDisabled: 'Current issue cannot switch to this state',
      allComplete: 'Showing all issues',
      empty: 'There are no issues here',
      btnSetFirst: 'Move it to the first',
      btnSetLast: 'Move it to the last'
    },
    className: {
      iconComment: 'icon comments outline',
      iconAngleLeft: 'icon angle double left',
      iconAngleRight: 'icon angle double right',
      iconIssue: 'icon sticky note outline',
      card: 'ui link card',
      action: 'ui button',
      actions: 'ui mini basic icon buttons',
      avatar: 'ui avatar image'
    },
    actions: function actions(config) {
      if (!config.plugins.Sortable) {
        return [];
      }

      return [{
        id: 'btn-set-first',
        "class": config.className.action,
        icon: config.className.iconAngleLeft,
        title: config.message.btnSetFirst,
        callback: function callback(boards, board) {
          var state = board.state.toString();
          var states = boards.sortable.toArray();
          var i = states.indexOf(state);

          if (i >= 0) {
            states.splice(i, 1);
            states.splice(0, 0, state);
            boards.sortable.sort(states);
            config.onSorted(states);
            boards.load();
          }
        }
      }, {
        id: 'btn-set-last',
        "class": config.className.action,
        icon: config.className.iconAngleRight,
        title: config.message.btnSetLast,
        callback: function callback(boards, board) {
          var state = board.state.toString();
          var states = boards.sortable.toArray();
          var i = states.indexOf(state);

          if (i >= 0) {
            states.splice(i, 1);
            states.push(state);
            boards.sortable.sort(states);
            config.onSorted(states);
            boards.load();
          }
        }
      }];
    },
    data: [{
      order: 1,
      name: 'Backlog',
      state: 'open',
      color: '#ffa726',
      issuesCount: 0
    }, {
      order: 2,
      name: 'Done',
      state: 'closed',
      color: '#2baf2b',
      issuesCount: 0
    }],
    plugins: {},
    types: [],
    user: {
      id: 0,
      admin: false
    },

    /**
     * 在任务列表加载完后的回调
     * @callback BoardLoadCallback
     * @param {Array} issues 任务列表
     * @param {Number} count 任务总数
     */

    /**
     * 在开始加载任务列表时的回调
     * @param {Object,Board} board 板块对象
     * @param {BoardLoadCallback} callback 用于接收任务列表的回调函数
     */
    onLoad: function onLoad(board, callback) {
      $.ajax({
        url: 'issues',
        data: {
          state: board.state,
          page: board.page
        }
      }).done(function () {
        callback([], 0);
      }).fail(function () {
        callback([], 0);
      });
    },

    /**
     * 在更新任务时的回调
     * @param {Object} issue 任务
     * @param {String,Number} oldState 更新前的状态
     */
    onUpdate: function onUpdate(issue, oldState) {
      var _this = this;

      $.ajax({
        type: 'PUT',
        url: issue.url,
        data: {
          state: issue.state
        }
      }).done(function (res) {
        _this.updateIssue(res);
      }).fail(function () {
        _this.setIssueState(issue.id, oldState);
      });
    },

    /**
     * 在渲染任务卡片时的回调
     * @param {Object} issue 任务
     * @param {JQuery} $el 任务卡片
     */
    onRender: function onRender(issue, $el) {
      $el.addClass("issue-state-".concat(issue.state));
      return $el;
    },

    /**
     * 在板块被排序后的回调
     * @param {Array} states 状态列表
     */
    onSorted: function onSorted(states) {
      window.console.log(states);
    }
  };
  var Config = function Config(config) {
    _classCallCheck(this, Config);

    $.extend(this, defaults, config);

    if (config.className) {
      this.className = $.extend({}, defaults.className, config.className);
    }

    if (config.message) {
      this.message = $.extend({}, defaults.message, config.message);
    }
  };

  var htmlSafe = function () {
    var $el = $('<div/>');
    return function (text) {
      return $el.text(text).html();
    };
  }();

  var Renderer =
  /*#__PURE__*/
  function () {
    function Renderer(config) {
      _classCallCheck(this, Renderer);

      this.config = config;
    }

    _createClass(Renderer, [{
      key: "getAvatarUrl",
      value: function getAvatarUrl(name, avatarUrl) {
        var Avatar = this.config.plugins.LetterAvatar;

        if (!avatarUrl || avatarUrl.indexOf('no_portrait.png') === 0) {
          if (Avatar) {
            return Avatar(name);
          }
        }

        return avatarUrl;
      }
    }, {
      key: "getCardId",
      value: function getCardId(issue) {
        if (this.config.getIusseId) {
          return this.config.getIusseId(issue);
        }

        return 'issue-card-' + issue.id;
      }
    }]);

    return Renderer;
  }();

  /* eslint-disable indent */

  function getUserUrl(user) {
    return user.html_url || user.path;
  }

  function renderCardUserLabel(issue) {
    if (!issue.author) {
      return '';
    }

    if (typeof issue.author.is_member !== 'undefined') {
      if (!issue.author.is_member) {
        return '<span class="user-label blue">[访客]</span>';
      }
    }

    if (issue.author.outsourced) {
      return '<span class="user-label red">[外包]</span>';
    }

    return '';
  }

  function renderCardLabels(issue) {
    if (!issue.labels) {
      return '';
    }

    var html = issue.labels.map(function (label) {
      return "<span class=\"label\" style=\"background-color: #".concat(label.color, "; color: #fff\">\n      ").concat(htmlSafe(label.name), "\n    </span>");
    }).join('');
    return "<div class=\"labels\">".concat(html, "</div>");
  }

  var CardRenderer =
  /*#__PURE__*/
  function (_Renderer) {
    _inherits(CardRenderer, _Renderer);

    function CardRenderer() {
      _classCallCheck(this, CardRenderer);

      return _possibleConstructorReturn(this, _getPrototypeOf(CardRenderer).apply(this, arguments));
    }

    _createClass(CardRenderer, [{
      key: "render",
      value: function render(issue) {
        var user = this.config.user;
        var readonly = this.config.readonly;
        var draggable = '';
        var cardClass = this.config.className.card;

        if (!readonly && (user.admin || user.id === issue.author.id)) {
          draggable = 'draggable="true"';
          cardClass += ' card-draggable';
        }

        return "<li class=\"".concat(cardClass, "\" ").concat(draggable, "\n      id=\"").concat(this.config.name, "-issue-").concat(issue.id, "\" data-id=\"").concat(issue.id, "\">\n      <div class=\"content\">\n        <a class=\"ui small header card-header\" href=\"").concat(issue.html_url, "\"\n        title=\"").concat(htmlSafe(issue.title), "\" target=\"_blank\">\n          ").concat(htmlSafe(issue.title), "\n        </a>\n      </div>\n      <div class=\"extra content\">\n        ").concat(this.renderAssignee(issue), "\n        ").concat(this.renderState(issue), "\n        <span class=\"card-number\">#").concat(issue.number, "</span>\n        <span class=\"card-author\">\n          by\n          <a class=\"card-author-name\" href=\"").concat(getUserUrl(issue.author), "\" target=\"_blank\">\n            ").concat(htmlSafe(issue.author.name), "\n          </a>\n        </span>\n        ").concat(renderCardUserLabel(issue), "\n        ").concat(this.renderComments(issue), "\n        ").concat(renderCardLabels(issue), "\n      </div>\n    </li>");
      }
    }, {
      key: "renderState",
      value: function renderState(issue) {
        var state = issue.state_data;

        if (!state || this.config.key === 'state') {
          return '';
        }

        return "<div class=\"state\">\n      <i title=\"".concat(state.name, "\" class=\"").concat(state.icon, "\" style=\"color: ").concat(state.color, "\"></i>\n    </div>");
      }
    }, {
      key: "renderAssignee",
      value: function renderAssignee(issue) {
        var user = issue.assignee;

        if (!user || !user.id || this.config.key !== 'state') {
          return '';
        }

        return "<div class=\"assignee\">\n        <a target=\"_blank\" href=\"".concat(getUserUrl(user), "\" title=\"").concat(htmlSafe(user.name), "\">\n          <img src=\"").concat(this.getAvatarUrl(user.name, user.avatar_url), "\"\n            alt=\"").concat(htmlSafe(user.name), "\" class=\"").concat(this.config.className.avatar, "\">\n        </a>\n      </div>");
      }
    }, {
      key: "renderComments",
      value: function renderComments(issue) {
        var className = this.config.className.iconComment;

        if (!issue.comments) {
          return '';
        }

        return "<span class=\"card-comments\"><i class=\"".concat(className, "\"></i>").concat(issue.comments, "</span>");
      }
    }]);

    return CardRenderer;
  }(Renderer);

  function setColorAlpha(color, alpha) {
    var reg = /^#([\da-fA-F]{2})([\da-fA-F]{2})([\da-fA-F]{2})$/;
    var matches = reg.exec(color);
    var r = parseInt(matches[1], 16);
    var g = parseInt(matches[2], 16);
    var b = parseInt(matches[3], 16);
    return "rgba(".concat(r, ",").concat(g, ",").concat(b, ",").concat(alpha, ")");
  }

  function renderBoardIcon(board) {
    var iconStyle = '';

    if (board.color) {
      iconStyle = "style=\"color: ".concat(board.color, "\"");
    }

    if (board.icon) {
      return "<i class=\"iconfont ".concat(board.icon, "\" ").concat(iconStyle, "></i>");
    }

    return '';
  }

  var BoardRenderer =
  /*#__PURE__*/
  function (_Renderer) {
    _inherits(BoardRenderer, _Renderer);

    function BoardRenderer() {
      _classCallCheck(this, BoardRenderer);

      return _possibleConstructorReturn(this, _getPrototypeOf(BoardRenderer).apply(this, arguments));
    }

    _createClass(BoardRenderer, [{
      key: "render",
      value: function render(board) {
        return "<div class=\"board\" data-state=\"".concat(board.state, "\">\n      <div class=\"board-inner\">\n        ").concat(this.renderHeader(board), "\n        <div class=\"board-list-wrapper\">\n          <ul class=\"board-list\"></ul>\n          <div class=\"ui inverted dimmer\">\n            <div class=\"ui loader\"></div>\n          </div>\n          <div class=\"board-blur-message\">\n            <p><i class=\"icon ban\"></i></p>\n            <p>").concat(this.config.message.stateDisabled, "</p>\n          </div>\n        </div>\n      </div>\n    </div>");
      }
    }, {
      key: "renderHeader",
      value: function renderHeader(board) {
        var headerStyle = '';
        var headerClass = 'board-header';

        if (board.color) {
          if (board.colorTarget === 'background') {
            headerStyle = "background-color: ".concat(setColorAlpha(board.color, 0.04));
          } else {
            headerClass += ' has-border';
            headerStyle = "border-color: ".concat(board.color);
          }
        }

        return "<div class=\"".concat(headerClass, "\" style=\"").concat(headerStyle, "\">\n      <h3 class=\"board-title\">\n        ").concat(this.renderActions(), "\n        <div class=\"right floated issues-count-badge\">\n          <i class=\"").concat(this.config.className.iconIssue, "\"></i>\n          <span class=\"issues-count\">").concat(board.issuesCount, "</span>\n        </div>\n        ").concat(this.renderAvatar(board), "\n        ").concat(renderBoardIcon(board), "\n        ").concat(htmlSafe(board.name), "\n      </h3>\n    </div>");
      }
    }, {
      key: "renderActions",
      value: function renderActions() {
        var config = this.config;
        var actions = config.actions;

        if (typeof actions === 'function') {
          actions = actions(config);
        }

        return "<div class=\"right floated board-actions ".concat(config.className.actions, "\">\n        ").concat(actions.map(function (a) {
          return "<button type=\"button\" class=\"board-action ".concat(a["class"], "\"\n            title=\"").concat(a.title, "\" data-id=\"").concat(a.id, "\">\n            <i class=\"").concat(a.icon, "\"></i>\n          </button>");
        }).join(''), "\n    </div>");
      }
    }, {
      key: "renderAvatar",
      value: function renderAvatar(board) {
        if (!board.avatarUrl) {
          return '';
        }

        return "<img class=\"".concat(this.config.className.avatar, " board-avatar\"\n      alt=\"").concat(board.state, "\" src=\"").concat(this.getAvatarUrl(board.name, board.avatarUrl), "\">");
      }
    }, {
      key: "renderTip",
      value: function renderTip(board) {
        var msg = this.config.message;

        if (board.loadable) {
          return "<li class=\"board-tip\">\n        <span class=\"ui active mini inline loader\"></span>\n        ".concat(msg.loading, "\n      </li>");
        }

        if (board.completed && board.issues.length > 0) {
          return "<li class=\"board-tip\">".concat(msg.allComplete, "</li>");
        }

        return "<li class=\"board-tip\">".concat(msg.empty, "</li>");
      }
    }]);

    return BoardRenderer;
  }(Renderer);
  var Board =
  /*#__PURE__*/
  function () {
    function Board(data, config) {
      var _this = this;

      _classCallCheck(this, Board);

      this.page = 0;
      this.loadable = true;
      this.loading = false;
      this.completed = false;
      this.issues = [];
      this.name = data.name;
      this.icon = data.icon;
      this.state = data.state;
      this.color = data.color;
      this.colorTarget = data.colorTarget;
      this.issuesCount = data.issuesCount || 0;
      this.avatarUrl = data.avatarUrl;
      this.config = config;
      this.renderer = new BoardRenderer(config);
      this.cardRenderer = new CardRenderer(config);
      this.$tip = null;
      this.$el = $(this.renderer.render(this));
      this.$dimmer = this.$el.find('.ui.dimmer');
      this.$issues = this.$el.find('.board-list');
      this.$issuesCount = this.$el.find('.issues-count');
      this.$issues.on('scroll', function () {
        _this.autoload();
      });
    }

    _createClass(Board, [{
      key: "add",
      value: function add(issue) {
        this.issues.push(issue);
        this.issuesCount += 1;
        this.$issuesCount.text(this.issuesCount);
      }
    }, {
      key: "find",
      value: function find(id) {
        for (var i = 0; i < this.issues.length; ++i) {
          var issue = this.issues[i];

          if (issue.id === id) {
            return i;
          }
        }

        return -1;
      }
    }, {
      key: "get",
      value: function get(id) {
        var i = this.find(id);

        if (i >= 0) {
          return this.issues[i];
        }

        return null;
      }
    }, {
      key: "remove",
      value: function remove(id) {
        var i = this.find(id);

        if (i >= 0) {
          this.issuesCount -= 1;
          this.$issuesCount.text(this.issuesCount);
          return this.issues.splice(i, 1);
        }

        return null;
      }
    }, {
      key: "clear",
      value: function clear() {
        this.page = 0;
        this.issues = [];
        this.issuesCount = 0;
        this.loading = false;
        this.loadable = true;
        this.completed = false;
        this.$issuesCount.text(0);
        this.$issues.empty();
        this.$tip = null;
      }
    }, {
      key: "autoload",
      value: function autoload() {
        if (this.completed) {
          return;
        }

        if (!this.$tip || this.$tip.position().top < this.$issues.height()) {
          this.load();
        }
      }
    }, {
      key: "load",
      value: function load() {
        if (this.loading) {
          return false;
        }

        this.page += 1;
        this.loading = true;

        if (this.page === 1) {
          this.$dimmer.addClass('active');
        }

        this.config.onLoad(this, this.onLoadDone.bind(this));
        return true;
      }
    }, {
      key: "onLoadDone",
      value: function onLoadDone(issues, count) {
        this.issuesCount = count;
        this.$issuesCount.text(this.issuesCount);
        this.appendIssues(issues);

        if (this.issuesCount > 0) {
          if (issues.length < 1 || this.issuesCount === issues.length) {
            this.loadable = false;
            this.completed = true;
          } else {
            this.loadable = true;
            this.completed = false;
          }
        } else {
          this.loadable = false;
          this.completed = true;
        }

        if (this.page === 1) {
          this.$dimmer.removeClass('active');
        }

        this.loading = false;
        this.updateTip();
        this.autoload();
      }
      /* 进行初次加载 */

    }, {
      key: "firstload",
      value: function firstload() {
        if (this.page > 0) {
          return false;
        }

        return this.load();
      }
      /**
       * 更新提示
      */

    }, {
      key: "updateTip",
      value: function updateTip() {
        if (this.$tip) {
          this.$tip.remove();
        }

        this.$tip = $(this.renderer.renderTip(this));
        this.$issues.append(this.$tip);
      }
    }, {
      key: "createCard",
      value: function createCard(issue) {
        var $card = $(this.cardRenderer.render(issue));
        var onSelect = this.config.onSelect;

        if (onSelect) {
          $card.on('click', function (e) {
            var $target = $(e.target);

            if (!$target.is('a')) {
              $target = $target.parent();
            }

            if (!$target.parent().hasClass('card-header') && $target.is('a') && $target.attr('href')) {
              return;
            }

            onSelect(issue, e);
          });
        }

        return this.config.onRender(issue, $card, this.config);
      }
    }, {
      key: "appendIssue",
      value: function appendIssue(issue) {
        this.issues.push(issue);
        this.$issues.append(this.createCard(issue));
      }
    }, {
      key: "prependIssue",
      value: function prependIssue(issue) {
        this.issuesCount += 1;
        this.issues.splice(0, 0, issue);
        this.$issues.prepend(this.createCard(issue));
        this.$issuesCount.text(this.issuesCount);
      }
    }, {
      key: "updateIssue",
      value: function updateIssue(issue) {
        var $issue = $("#".concat(this.config.name, "-issue-").concat(issue.id));

        if ($issue.length < 1) {
          this.prependIssue(issue);
          return;
        }

        $issue.before(this.createCard(issue)).remove();
      }
      /**
       * 直接追加多个任务，不更新任务总数
       * @param {Array} issues 任务列表
       */

    }, {
      key: "appendIssues",
      value: function appendIssues(issues) {
        var _this2 = this;

        issues.filter(function (issue) {
          return _this2.issues.every(function (boardIssue) {
            return boardIssue.id !== issue.id;
          });
        }).forEach(function (issue) {
          issue[_this2.config.Key] = _this2.state;

          _this2.appendIssue(issue);
        });
      }
    }]);

    return Board;
  }();

  var Boards =
  /*#__PURE__*/
  function () {
    /**
     * 创建一个看板
     * @param {JQuery} $el 看板容器元素
     * @param {BoardsSettings} options 配置
     */
    function Boards($el, options) {
      _classCallCheck(this, Boards);

      var config = new Config(options);
      this.$el = $el;
      this.config = config;
      this.boards = {};
      this.$hoverIssue = null;
      this.$selectedIssue = null;
      this.timerForScrollLoad = null;
      this.bindDrag();
      this.bindScroll();
      this.initPlugins();
      this.setData(config.data);
    }

    _createClass(Boards, [{
      key: "initPlugins",
      value: function initPlugins() {
        this.initSortable();
      } // 初始化拖拽排序功能

    }, {
      key: "initSortable",
      value: function initSortable() {
        var that = this;
        var Sortable = that.config.plugins.Sortable; // 如果没有注入 SortableJS 依赖，则不启用这个功能

        if (!Sortable) {
          return false;
        }

        that.$el.addClass('boards-sortable');
        that.sortable = Sortable.create(that.$el[0], {
          handle: '.board-title',
          dataIdAttr: 'data-state',
          onUpdate: function onUpdate() {
            that.config.onSorted(that.sortable.toArray());
          }
        });
        that.$el.on('click', '.board-action', function (e) {
          var $target = $(this);
          var id = $target.data('id');
          var state = $target.parents('.board').data('state');
          var actions = that.config.actions;

          if (typeof actions === 'function') {
            actions = actions(that.config);
          }

          actions.some(function (action) {
            if (action.id === id && action.callback) {
              action.callback(that, that.boards[state], e);
              return true;
            }

            return false;
          });
        });
        return true;
      }
    }, {
      key: "bindScroll",
      value: function bindScroll() {
        var that = this;
        var timerForScrollLoad = null;

        function onScrollLoad() {
          if (timerForScrollLoad) {
            clearTimeout(timerForScrollLoad);
          }

          timerForScrollLoad = setTimeout(function () {
            timerForScrollLoad = null;
            that.load();
          }, 200);
        }

        $(window).on('resize', onScrollLoad);
        this.$el.on('scroll', onScrollLoad);
      }
    }, {
      key: "bindDrag",
      value: function bindDrag() {
        var that = this;

        if (that.config.readonly) {
          return;
        }

        that.$el.on('dragstart', '.card', function (e) {
          var issueId = $(this).data('id');
          var issue = that.getIssue(issueId);

          if (issue) {
            that.setFocus(issue.type, issue.state);
          }

          that.$selectedIssue = $(this);
          e.originalEvent.dataTransfer.setData('text/plain', issueId);
          e.stopPropagation();
        });
        that.$el.on('dragend', '.card', function () {
          that.clearFocus();
        });
        that.$el.on('drop', '.board-list', function (e) {
          e.preventDefault();
          e.stopPropagation();

          if (that.$hoverIssue) {
            that.$hoverIssue.removeClass('card-dragover');
          }

          if (!that.$selectedIssue) {
            return false;
          }

          if (that.config.readonly) {
            return false;
          }

          var $issue = that.$selectedIssue;
          var issueId = $issue.data('id');
          var $board = $(this).parents('.board');
          var state = $board.data('state');
          var oldState = $issue.parents('.board').data('state');
          var nextIssueId = that.$hoverIssue ? that.$hoverIssue.data('id') : null;
          that.setIssueState(issueId, state, nextIssueId, function _callee(issue) {
            return regeneratorRuntime.async(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    return _context.abrupt("return", that.config.onUpdate(issue, oldState, nextIssueId));

                  case 1:
                  case "end":
                    return _context.stop();
                }
              }
            });
          });
          return true;
        });
        that.$el.on('dragover', '.board-list', function (e) {
          var key = that.config.key;
          var $target = $(e.target);

          if (!that.$selectedIssue) {
            return;
          }

          e.preventDefault();

          while ($target.length > 0 && !$target.hasClass('card')) {
            $target = $target.parent();
          }

          if ($target.length < 1) {
            if (that.$hoverIssue) {
              that.$hoverIssue.removeClass('card-dragover');
            }

            that.$hoverIssue = null;
            return;
          }

          if (that.$hoverIssue) {
            that.$hoverIssue.removeClass('card-dragover');
          }

          that.$hoverIssue = $target;
          var hoverIssue = that.getIssue($target.data('id'));
          var selectedIssue = that.getIssue(that.$selectedIssue.data('id'));

          if (hoverIssue && selectedIssue && hoverIssue[key] !== selectedIssue[key]) {
            that.$hoverIssue.addClass('card-dragover');
          }
        });
      }
      /**
       * 设置焦点板块
       * 根据指定的类型和状态，排除无关状态的板块
       * @param {String, Number} typeId 任务类型 id
       * @param {String, Number} stateId 任务状态 id
       */

    }, {
      key: "setFocus",
      value: function setFocus(typeId, stateId) {
        var _this = this;

        var stateIds = [];
        var issueType = null;

        if (this.config.key !== 'state') {
          return;
        }

        this.config.types.some(function (t) {
          if (t.id === typeId) {
            issueType = t;
            return true;
          }

          return false;
        });

        if (issueType) {
          stateIds = issueType.states.map(function (state) {
            return state.id.toString();
          });
        }

        Object.keys(this.boards).forEach(function (id) {
          if (id !== stateId.toString() && stateIds.indexOf(id) === -1) {
            _this.boards[id].$el.addClass('board-blur');
          }
        });
      }
      /* 清除板块的焦点效果 */

    }, {
      key: "clearFocus",
      value: function clearFocus() {
        var _this2 = this;

        Object.keys(this.boards).forEach(function (key) {
          _this2.boards[key].$el.removeClass('board-blur');
        });
      }
    }, {
      key: "getIssueCard",
      value: function getIssueCard(id) {
        return $("#".concat(this.config.name, "-issue-").concat(id));
      }
    }, {
      key: "getIssue",
      value: function getIssue(id) {
        var $issue = this.getIssueCard(id);
        var $board = $issue.parents('.board');
        var state = $board.data('state');
        var board = this.boards[state];

        if (board) {
          return board.get(id);
        }

        return null;
      }
    }, {
      key: "removeIssue",
      value: function removeIssue(id) {
        var issue = null;
        var $issue = this.getIssueCard(id);

        if ($issue.length < 1) {
          return issue;
        }

        var state = $issue.parents('.board').data('state');

        if (state) {
          var board = this.boards[state];

          if (board) {
            issue = board.remove(id);
          }
        }

        $issue.remove();
        return issue;
      }
    }, {
      key: "updateIssue",
      value: function updateIssue(issue) {
        var board = this.boards[issue[this.config.key]];

        if (board) {
          board.updateIssue(issue);
          return true;
        }

        return false;
      }
    }, {
      key: "prependIssue",
      value: function prependIssue(issue) {
        var board = this.boards[issue[this.config.key]];

        if (board) {
          board.prependIssue(issue);
          return true;
        }

        return false;
      }
    }, {
      key: "setIssueState",
      value: function setIssueState(issueId, state, nextIssueId, callback) {
        var $issue = this.getIssueCard(issueId);
        var $nextIssue = nextIssueId ? this.getIssueCard(nextIssueId) : null;

        if ($issue.length < 1) {
          return null;
        }

        var user = this.config.user;
        var $oldBoard = $issue.parents('.board');
        var oldState = $oldBoard.data('state');
        var oldBoard = this.boards[oldState];
        var newBoard = this.boards[state];

        if (oldBoard.state === state) {
          return null;
        } // 如果新的板块不接受该状态的 issue


        if (newBoard.exclude && newBoard.exclude.indexOf(oldState) >= 0) {
          return null;
        }

        var issue = oldBoard.get(issueId); // 如果当前用户既不具备管理权限，也不是 issue 作者，则禁止操作

        if (!user.admin && issue.author.id !== user.id) {
          return null;
        }

        issue[this.config.key] = state;
        newBoard.add(issue);
        oldBoard.remove(issue.id);
        $issue.hide(256, function () {
          // 如果有指定下一个 issue，则将当前 issue 插入到它前面
          if ($nextIssue) {
            $nextIssue.before($issue);
          } else {
            newBoard.$issues.prepend($issue);
          }

          $issue.addClass('card-loading');
          $issue.show(256, function _callee2() {
            return regeneratorRuntime.async(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    if (!callback) {
                      _context2.next = 3;
                      break;
                    }

                    _context2.next = 3;
                    return regeneratorRuntime.awrap(callback(issue));

                  case 3:
                    $issue.removeClass('card-loading');

                  case 4:
                  case "end":
                    return _context2.stop();
                }
              }
            });
          });
        });
        newBoard.updateTip();
        oldBoard.updateTip();
        oldBoard.autoload();
        return issue;
      }
    }, {
      key: "load",
      value: function load() {
        var _this3 = this;

        var count = 0;
        var bound = this.$el.offset(); // 设置边界框（可见区域）的尺寸

        bound.width = this.$el.width();
        bound.height = this.$el.height();
        Object.keys(this.boards).forEach(function (state) {
          var board = _this3.boards[state];
          var offset = board.$el.offset(); // 如果当前板块在可见区域内

          if (offset.top + board.$el.height() > bound.top && offset.left + board.$el.width() > bound.left && offset.top < bound.top + bound.height && offset.left < bound.left + bound.width) {
            if (board.firstload()) {
              count += 1;
            }
          }
        });
        return count;
      }
    }, {
      key: "setData",
      value: function setData(data) {
        var _this4 = this;

        this.boards = {};
        this.$el.addClass('boards-list').empty();
        data.forEach(function (boardData) {
          var board = new Board(boardData, _this4.config);
          _this4.boards[boardData.state] = board;

          _this4.$el.append(board.$el);
        });
      }
    }, {
      key: "clearData",
      value: function clearData() {
        var _this5 = this;

        Object.keys(this.boards).forEach(function (state) {
          _this5.boards[state].clear();
        });
      }
    }]);

    return Boards;
  }();

  $.fn.boards = function (options) {
    var that = this.data('boards');
    var settings = $.extend({}, $.fn.boards.settings, options);

    if (!that) {
      that = new Boards(this, settings);
      this.data('boards', that);
      that.load();
    }

    return this;
  };

  $.fn.boards.settings = defaults;

}());
